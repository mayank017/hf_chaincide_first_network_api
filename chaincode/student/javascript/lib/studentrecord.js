/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');
const ClientIdentity = require('fabric-shim').ClientIdentity;

class StudentRecord extends Contract {

    
    async initLedger(ctx) {
        console.info('============= START : Initialize Ledger ===========');
        const infos = [
            {
                //email is to unique for the students.
                email: 'student@gmail.com',
                firstName: 'student',
                lastName: 'student',
                phoneNo: '1234567890',
                id : 'student@gmail.com',
            }
        ];

        for (let i = 0; i < infos.length; i++) {
            // infos[i].docType = 'car';
            await ctx.stub.putState(infos[i]['email'], Buffer.from(JSON.stringify(infos[i])));
            console.info('Added <--> ', infos[i]);
        }
        console.info('============= END : Initialize Ledger ===========');
    }

    async queryStudent(ctx, email) {
        if (email.length <= 0) {
            throw new Error(`Please provide email param!`);
        }
        let cid = new ClientIdentity(ctx.stub); // "stub" is the ChaincodeStub object passed to Init() and Invoke() methods
        var callerMspid = cid.getMSPID()
        console.info("callerMspid--MSPID--", callerMspid);
        var callerid = cid.getID()
        console.info("callerid", callerid)
        var callerCertificate = cid.getX509Certificate()
        console.info("callerCertificate", callerCertificate)
        const studentAsBytes = await ctx.stub.getState(email); // get the car from chaincode state
        if (!studentAsBytes || studentAsBytes.length === 0) {
            throw new Error(`${email} does not exist`);
        }
        console.info(studentAsBytes.toString());
        return studentAsBytes.toString();
    }

    async addStudentRecord(ctx, phoneNo,email, lastName, firstName) {

        if (lastName.length <= 0) {
            throw new Error(`Please provide lastName param!`);
        }
        if (phoneNo.length != 10) {
            throw new Error(`Please provide phoneNo param!`);
        }
        if (email.length <= 0) {
            throw new Error(`Please provide email param!`);
        }
        if (firstName.length <= 0) {
            throw new Error(`Please provide firstName param!`);
        }     
        
        console.info('============= START : addStudentRecord ===========');
        const studentAsBytes = await ctx.stub.getState(email); // get the student from chaincode state
        console.info('============= studentAsBytes ===========', studentAsBytes);

        if (studentAsBytes && studentAsBytes.length > 0) {
            throw new Error(`${email} already exist`);
        }
        const student = {
            id: email,
            lastName,
            firstName,
            phoneNo,
            email
        };

        await ctx.stub.putState(email, Buffer.from(JSON.stringify(student)));
        console.info('============= END : addStudentRecord ===========');
    }

    async queryAllStudents(ctx) {
        const startKey = '';
        const endKey = '';

        const iterator = await ctx.stub.getStateByRange(startKey, endKey);

        const allResults = [];
        while (true) {
            const res = await iterator.next();

            if (res.value && res.value.value.toString()) {
                console.log(res.value.value.toString('utf8'));

                const Key = res.value.key;
                let Record;
                try {
                    Record = JSON.parse(res.value.value.toString('utf8'));
                } catch (err) {
                    console.log(err);
                    Record = res.value.value.toString('utf8');
                }
                allResults.push({ Key, Record });
            }
            if (res.done) {
                console.log('end of data');
                await iterator.close();
                console.info(allResults);
                return JSON.stringify(allResults);
            }
        }
    }

    async getHistoryForUserStudent(ctx, email) {
        if (email.length <= 0) {
            throw new Error(`Please provide email param!`);
        }
        console.info('============= START : getHistoryForUserStudent ===========', email);
        if (email.length <= 0) {
            throw new Error('email is required to fetch the  history');
        }
        let iterator = await ctx.stub.getHistoryForKey(email);

        let allResults = [];
        while (true) {
            let res = await iterator.next();
            console.info('============= KEY___START========');

            if (res.value && res.value.value.toString()) {
                let jsonRes = {};
                console.info(res.value.value.toString('utf8'));
                console.info('---value--', res.value.value.toString('utf8'));
                console.info('---res.value--', res.value);
                let txId = res.value.tx_id;
                let timestamp = res.value.timestamp;
                let Key = res.value.key;
                console.info('---Key--', Key);
                jsonRes.txId = txId
                jsonRes.timestamp = timestamp
                jsonRes.Key = res.value.key;
                try {
                    jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
                }
                catch (err) {
                    console.info('----ERROR---', err);
                    jsonRes.Record = res.value.value.toString('utf8');
                }
                allResults.push(jsonRes);
                console.info('============= KEY___END========');

            }
            if (res.done) {
                console.info('============= END : getHistoryForUserStudent ===========');
                await iterator.close();
                console.info(allResults);
                return JSON.stringify(allResults);
            }
        }

    }

}

module.exports = StudentRecord;
