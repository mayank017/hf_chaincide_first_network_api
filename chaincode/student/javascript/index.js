/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const StudentRecord = require('./lib/studentrecord');

module.exports.StudentRecord = StudentRecord;
module.exports.contracts = [ StudentRecord ];
